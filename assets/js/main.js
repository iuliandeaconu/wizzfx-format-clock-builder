document.addEventListener("DOMContentLoaded", function(){
    interface.addbtn(); //initializes the add button
    interface.downloadbtn(); // initializes the edit button
    interface.editbtn();
    interface.delbtn();
    interface.confirmDel();
    
    // clears sessionStorage at start
    if (sessionStorage.getItem("rotation") != null)
    sessionStorage.clear();


});

// prevents the user from accidentally exiting
window.onbeforeunload = function(){
    if (myStorage.getJSON('rotation').length !=0) {
    return "Are you sure you want to close the window?";
}}


// Checks if the time is exceeded
function checkExceeded (element, existingmin, existingsec) {
    var time = myStorage.getJSON('time');
    var min = element.minutes*60;
    var sec = element.seconds;
    console.log('Existing time: ', time);
    console.log('minutes: ' ,element.minutes );
    console.log(time + min + sec)
    if (time - existingmin - existingsec + min + sec > 3600) {
        return true;
    } 
        else {
            sessionStorage.setItem('exceeded', 'false');
            return false;

        }
}


function downloadChart() {
    var canvas = document.getElementById('fullResChart');
    
    
    if (canvas.toBlob) {
    canvas.toBlob(
        function (blob) {
            var date =  new Date();;
            var day = date.getDate();
            var month = date.getMonth();
            var year = date.getFullYear();
            var hour = date.getHours();
            var minutes = date.getMinutes();
            var filename = 'chart ' + day + '_' + month + '_' + year + "_" + hour + "_" + minutes + '.png'; 
            saveAs(blob, filename);
        },
        'image/png'
    );
}      
};

// Global arrays for ChartJS
var labels = [];
var dataset = [];
var colors = [];

// Load existing data into the editing fields if a new element is selected
document.getElementById('editList'). onchange = function() {
    getElementInfo();
}


// storage constructor 
class StorageManager {
    constructor(storage) {
        this.storage = storage;
    }

    getItem(key, defaultValue) {
        return this.storage.getItem(key) || defaultValue;
    }

    setItem(key, string) {
        try {
            this.storage.setItem(key, string);
            return true;
        } catch (e) {
            console.error(e);
            return false;
        }
    }

    removeItem(key) {
         this.storage.removeItem(key);
    }

    getJSON(key, defaultValue) {
        let result = null;

        try {
            result = JSON.parse(this.storage.getItem(key));
        } catch (e) {
            console.error(e);
        }

        return result !== null ? result : defaultValue;
    }

    setJSON(key, object) {
        return this.setItem(key, JSON.stringify(object));
    }
}


const myStorage = new StorageManager(sessionStorage);

// CHeck if rotation is empty 
function ifRotEmpty() {
    // enable the edit & download pills
    if (myStorage.getJSON('rotation').length !=0) {
        $("#editrotbtnpill").attr("data-toggle","pill");
        $("#editrotbtnpill").attr("href","#edit");
        $("#editrotbtn").removeAttr('class');
        $("#downloadlink").removeAttr('class');

    }
        else {
            $("#editrotbtnpill").removeAttr("data-toggle");
            $("#editrotbtnpill").removeAttr("href");
            $('.nav-pills a[href="#add"]').tab('show'); // return to the add tab
            $("#editrotbtn").attr('class', 'disabled');
            $("#downloadlink").attr('class', 'disabled');

        }
}

// Delete element
function deleteItem() {
    // gets the data
    var itemList = document.getElementById('editList');
    var selected = document.getElementById('editList').value;
    var rotArray = myStorage.getJSON('rotation');
    var editList = document.getElementById('editList');
    var time = myStorage.getJSON('time');

    // deletes the data
    labels.splice(selected,1);
    dataset.splice(selected,1);
    colors.splice(selected, 1);
    time =  time - (rotArray[selected].minutes*60 + rotArray[selected].seconds);
    rotArray.splice(selected, 1);
    editList.remove(editList.selectedIndex);

    // asign new IDs to each existing element after deleting one

    for (var i=0; i<itemList.options.length; i++) {
        itemList.options.item(i).value = i;
    }
    
    // stores the changes
    myStorage.setJSON('names', labels);
    myStorage.setJSON('dataset', dataset);
    myStorage.setJSON('colors', colors);
    myStorage.setJSON('rotation', rotArray);
    myStorage.setJSON('time', time);

    // updates the chart and the time counter
    chart1.update();
    text.escape(timeH.update());

    // closes the modal
    $("#delModal").modal('hide');

    ifRotEmpty();

// load the information of another element into the fields if the rotation isn't empty
if (myStorage.getJSON('rotation').length != 0) {
    getElementInfo();
}

} 

function editAfterClick() {
    var chosenType = document.getElementById("editType").value;
    switch(chosenType) {
        case "Ad break":
            editElement("Ad break", "#F78678");
            break;
            
        case "In to break":
            editElement("In to break","#DE798C");
            break;
            
        case "News":
           editElement("News", "#F78FB1");
            break;
        
        case "Out of break":
            editElement("Out of break", "#E79BCC");
            break;
            
        case "Promotion":
            editElement("Promotion", "#CDA9E1");
            break;
            
        case "Song":
            editElement("Song", "#ABB8EE");
            break;
        
        case "Song Intro":
            editElement("Song Intro", "#85C6F0");
            break;
        
        case "Sponsorship":
            editElement("Sponsorship", "#5ED1E9");
            break;
        
        case "Sport":
            editElement("Sport", "#46DAD8");
            break;
        
        case "Sweeper":
            editElement("Sweeper", 10, "#4FE1C0");
            break;
        
        case "Top of Hour":
            editElement("Top of Hour", "#6EE5A4");
            break;
            
        case "Voicebreak":
            editElement("Voicebreak","#94E689");
            break;
            
        case "Travel":
            editElement("Travel", "#BBE471");
            break;
        
        case "Weather":
            editElement("Weather", "#E2E062");
     

    }
}

function editElement(type, color) {
    var newType = type;
    var newColor = color; 
    var newLabel;
    var newMinutes;
    var newSeconds;
    var element = {};
    var rotArray = myStorage.getJSON('rotation');
    selected = document.getElementById('editList').value;

    var editItem = {
        getNewInfo: function getNewInfo() {
            newLabel = document.getElementById('editLabel').value;
            newMinutes = document.getElementById('editMinutes').value;
            newSeconds = document.getElementById('editSeconds').value;
            element.minutes = parseInt(newMinutes,10);
            element.seconds = parseInt(newSeconds,10);
            newData = element.minutes*60 + element.seconds;
        },

        editChart: function editChartInfo() {
            // check if the new label exists
            if(document.getElementById("editLabel").value !="") {
            labels[selected] = newType + ' - ' + newLabel + ' (' + timeH.addPrefix(element.minutes) + ':' + timeH.addPrefix(element.seconds)+ ')';
            } else {
                labels[selected] = newType  + ' (' + timeH.addPrefix(element.minutes) + ':' + timeH.addPrefix(element.seconds)+ ')';
            }
            dataset[selected] = newData;
            colors[selected] = newColor;
        },

        storeNewInfo: function storeNewInfo() {
            myStorage.setJSON('names', labels);
            myStorage.setJSON('dataset', dataset);
            myStorage.setJSON('colors', colors);
            myStorage.setJSON('rotation', rotArray);

        },

        editTime: function editTime() {
            var time = myStorage.getJSON('time');
            
            if(checkExceeded(element, rotArray[selected].minutes*60, rotArray[selected].seconds) == false) {
                time = time - (rotArray[selected].minutes*60) - rotArray[selected].seconds;
                time = time + (parseInt(newMinutes, 10)*60) + parseInt(newSeconds, 10);
                
        }
                

            myStorage.setJSON('time', time);

        },

        editInRotation: function editInRotation() {
            rotArray[selected].type = newType;
            rotArray[selected].data = newData;
            rotArray[selected].color = newColor;
            rotArray[selected].label = newLabel;
            rotArray[selected].minutes = parseInt(newMinutes, 10);
            rotArray[selected].seconds = parseInt(newSeconds, 10);
        }, 

        editInList: function editInList() {
            var x = document.getElementById('editList');
            x.remove(x.selectedIndex);
            var newoption = document.createElement("option");
            newoption.text = newType + ' - ' + newLabel + ' (' + timeH.addPrefix(element.minutes) + ':' + timeH.addPrefix(element.seconds)+ ')';
            newoption.value = selected;
            x.add(newoption, x[selected]);
        }
    }

    editItem.getNewInfo();
    if (checkExceeded(element, rotArray[selected].minutes*60, rotArray[selected].seconds) == false) {
        editItem.editChart();
        editItem.editTime();
        text.escape(timeH.update());
        editItem.editInRotation();
        editItem.storeNewInfo();
        editItem.editInList();
        chart1.update();
        document.getElementById('editelement').reset();
        getElementInfo();
    }
    else {
        $("#exceeded").modal("show");
    }
}

function getElementInfo() {
    var rotation = myStorage.getJSON('rotation');
    var selected = document.getElementById('editList').value;
    var editList = document.getElementById('editList');
    document.getElementById('editType').value = rotation[selected].type;
    document.getElementById('editLabel').value = rotation[selected].label;
    document.getElementById('editMinutes').value = rotation[selected].minutes;
    document.getElementById('editSeconds').value = rotation[selected].seconds;

}

function addAfterClick() {
    var chosenType = document.getElementById("addType").value;
    switch(chosenType) {
        case "Ad break":
            addElement("Ad break", "#F78678");
            break;
            
        case "In to break":
            addElement("In to break", "#DE798C");
            break;
            
        case "News":
           addElement("News", "#F78FB1");
            break;
        
        case "Out of break":
            addElement("Out of break", "#E79BCC");
            break;
            
        case "Promotion":
            addElement("Promotion", "#CDA9E1");
            break;
            
        case "Song":
            addElement("Song", "#ABB8EE");
            break;
        
        case "Song Intro":
            addElement("Song Intro", "#85C6F0");
            break;
        
        case "Sponsorship":
            addElement("Sponsorship", "#5ED1E9");
            break;
        
        case "Sport":
            addElement("Sport", "#46DAD8");
            break;
        
        case "Sweeper":
            addElement("Sweeper", "#4FE1C0");
            break;
        
        case "Top of Hour":
            addElement("Top of Hour",  "#6EE5A4");
            break;
            
        case "Voicebreak":
            addElement("Voicebreak","#94E689");
            break;
            
        case "Travel":
            addElement("Travel", "#BBE471");
            break;
        
        case "Weather":
            addElement("Weather", "#E2E062");
     

    }
} 

function addElement (type, color) { // main function for adding elements
    var itemType = type;
    var itemColor = color;
    var rotArray;
    var element = {};
    var newitem = { // newitem method
    add:    function addElement() {
            newitem.get();
            if (checkExceeded(element, 0, 0)==false) {
                newitem.store();
                newitem.count();
                chart1.update();
                newitem.time();
                text.escape(timeH.update());
                document.getElementById('addElementForm').reset();
                newitem.toList();
                ifRotEmpty();

            } else {
                $("#exceeded").modal("show");

            }

    },
    
    get:    function getProp() { //
                element.type = itemType;
                element.color = itemColor;
                element.label = document.getElementById("newlabel").value;
                var inputMinutes = document.getElementById("newminutes").value;
                element.minutes = parseInt(inputMinutes, 10);
                var inputSeconds = document.getElementById("newseconds").value;
                element.seconds = parseInt(inputSeconds, 10);
                element.data = element.minutes*60 + element.seconds;
    },
    
    store:  function storeProp() {
                if (sessionStorage .getItem('rotation') === null) {
                    rotArray = [];
                }
                    else {
                        rotArray = JSON.parse(sessionStorage .getItem('rotation'));
                    }
                pushToArray(rotArray, 'rotation', element);
        // checks if the new label field is empty
        if (document.getElementById("newlabel").value == "") {
                pushToArray(labels, 'names', element.type + ' (' + timeH.addPrefix(element.minutes) + ':' + timeH.addPrefix(element.seconds)+ ')');
        }
        else { 
            pushToArray(labels, 'names', element.type + ' - ' + element.label + ' (' + timeH.addPrefix(element.minutes) + ':' + timeH.addPrefix(element.seconds)+ ')');
        
    }
                pushToArray(dataset, 'dataset', element.data);
                pushToArray(colors, 'colors', element.color);
               
        
    },
        count: function countElements() {
                counter = rotArray.length-1;
                return counter;
        },
        
        time: function addTime() {
            var oldtime;
            var time = 0;
            if (sessionStorage .getItem('time') === null) {
                oldtime = 0;
                
            }
            else {
                oldtime = JSON.parse(sessionStorage .getItem('time'));
            }
            
            if (checkExceeded(element, 0,0) == false) {           
            time = oldtime + (element.minutes*60) + element.seconds;
            myStorage.setJSON('time', time);
        }
            else 
                alert ('exceeded');
            
        },
        
        toList: function addToList() {
                var list = document.getElementById("editList");
                list.options[list.options.length] = new Option(element.type + ' - ' + element.label + ' (' + timeH.addPrefix(element.minutes) + ':' + timeH.addPrefix(element.seconds)+ ')', newitem.count());
        
        }
        
        
}   
    newitem.add();
    getElementInfo();

    }

var text = {
    // Escapes HTML safely
    escape: function escapeHtml(str) {
            var div = document.getElementById('rotationtext');
            while (div.firstChild)
                div.removeChild(div.firstChild);
            div.appendChild(document.createTextNode(str));
            return div.innerHTML;
    }
}

var validate = {
    add: function validateAdd() {
                if($("#addElementForm")[0].checkValidity()) {
        return true;
    }
    else {
        $("#addElementForm")[0].reportValidity();
        return false;
    }
},
        edit: function validateEdit() {
                if($("#editelement")[0].checkValidity()) {
        return true;
    }
    else {
        $("#editelement")[0].reportValidity();
        return false;
    }
} 
    
    }

var timeH = {
    update : function updateTime() {
            var min_placeholder;
            var sec_placeholder;
            var totalTime = myStorage.getJSON('time');
            var minutes = Math.trunc(totalTime/60);
            var seconds = Math.trunc(totalTime%60);
            var textTime;
        
            // testing if the nouns should be either singular or plural
            if (minutes == 1) {
                min_placeholder = 'minute';
            }
                else {
                    min_placeholder = 'minutes';
                }
        
            if (seconds == 1) {
                sec_placeholder = 'second';
            }
                else {
                sec_placeholder = 'seconds';
                }    
        
            // testing if minutes/seconds should be visible
            if (minutes == 0) {
                textTime = seconds + ' ' + sec_placeholder;
            }
                    else if (seconds == 0) {
                        textTime =  minutes + ' ' + min_placeholder
                    }
            else {
                textTime =  minutes + ' ' + min_placeholder + ' and ' + seconds + ' ' + sec_placeholder;
            }
            
            return textTime;
    }, 

    addPrefix: function addPrefix(x) {
        if (x<10) {
            return '0' + x;
        }
            else {
                return x;
            }
    }

}

 function pushToArray (array, key, obj) {
            array.push(obj);
            myStorage.setJSON(key, array);
        }

var interface = {
    addbtn: function controlAddBtn() {
        document.getElementById("addbtn").onclick = function addBtnClicked() {
        if (validate.add() != false)
        addAfterClick();
    }
    },
    downloadbtn: function controlDownloadBtn() {

        document.getElementById("downloadbtn").onclick = function downloadBtnClicked() {
            if (myStorage.getJSON('rotation').length != 0) {
        document.getElementById("fullChart").style.display = "block";
        chart2.update();
        if (document.getElementById("fullChart").style.display != "none") {
        downloadChart();
        }
        document.getElementById("fullChart").style.display = 'none';}

    }}, 

     editbtn: function controlEditBtn() {
            document.getElementById("editbtn").onclick = function editBtnClicked() {
                if (validate.edit() != false)
                editAfterClick();
            }
    }, 

    delbtn: function controlEditBtn() {
        document.getElementById("delbtn").onclick = function delBtnClicked() {
            $('#delModal').modal('show');
            }
    },

    confirmDel: function controlConfirmDel() {
        document.getElementById('delConfirm').onclick = function delConfirmed() {
            deleteItem();
        }
    }
}





            // Preview chart
    var ctx1 = document.getElementById('myChart');
    //pie chart data
    var data1 = {
        labels:  labels,
        datasets: [
            {
                data: dataset, 
                backgroundColor: colors,
                
            }
        ]
    };

    //options
    var prev_options = {
        responsive: true,
        title: {
            display: false,
            position: "top",
            text: "Pie Chart",
            fontSize: 18,
            fontColor: "#111"
        },
        legend: {
            display: false,
            position: "bottom",
            labels: {
                fontColor: "#333",
                fontSize: 16
            }
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    var allData = data.datasets[tooltipItem.datasetIndex].data;
                    var tooltipLabel = data.labels[tooltipItem.index];
                    return tooltipLabel;
                }
            }
        }
    
    };

    //create Chart class object
    var chart1 = new Chart(ctx1, {
        type: "pie",
        data: data1,
        options: prev_options
    });


            // Full chart
    var ctx2 = document.getElementById('fullResChart');
    //pie chart data
    var data2 = {
        labels:  labels,
        datasets: [
            {
                data: dataset, 
                backgroundColor: colors,
                
            }
        ]
    };

    //options
    var full_options = {
        responsive: false,
        animation: false,
        title: {
            display: false,
            position: "bottom",
            text: "powered by wizzFX Format clock builder",
            fontSize: 18,
            fontColor: "#111"
        },
        legend: {
            display: true,
            position: "bottom",
            labels: {
                fontColor: "#333",
                fontSize: 16
            }
        }
    };


Chart.plugins.register({
  beforeDraw: function(chartInstance) {
    var ctx = chartInstance.chart.ctx;
    ctx.fillStyle = "white";
    ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
  }
});

    //create Chart class object
    var chart2 = new Chart(ctx2, {
        type: "pie",
        data: data2,
        options: full_options
    });


        